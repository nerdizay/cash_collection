FROM python:3.12.3-alpine3.18
WORKDIR /usr/src/app
RUN pwd
ENV PYTHONDONTWRITEBYTECODE 1 \
    PYTHONUNBUFFERED 1
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev
RUN pip install --upgrade pip
RUN pip install "poetry==1.6.1"
COPY pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.create false
RUN poetry install --no-root --no-interaction --no-ansi
# На случай запуска в прод режиме
# RUN poetry install --no-dev --no-root --no-interaction --no-ansi
COPY . .