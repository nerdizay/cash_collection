from django.contrib import admin
from .models import Collect, Payment


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    readonly_fields = (
        'date_time',
        'user',
        'amount',
        'description',
        'collect'
    )
    
    def has_delete_permission(self, request, obj=None):
        return False
    
@admin.register(Collect)
class CollectAdmin(admin.ModelAdmin):
    readonly_fields = (
        'author',
        'name',
        'reason',
        'description',
        'money_target',
        'money_now',
        'count_participant',
        'poster',
        'date_time_ending'
    )
    
    def has_delete_permission(self, request, obj=None):
        return False


admin.site.site_header = "Админ панель приложения сбора денежных средств"
