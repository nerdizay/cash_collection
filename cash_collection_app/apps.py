from django.apps import AppConfig


class CashCollectionAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cash_collection_app'
    verbose_name = 'Сбор денежных средств'
