from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
import random
from faker import Faker

from cash_collection_app.models import Collect


class Command(BaseCommand):
    help = 'Create fake collects'

    def handle(self, *args, **options):
        count_collects = int(input('Введите кол-во сборов: '))
        if count_collects <= 0:
            raise CommandError('Кол-во сборов не может быть меньше 0')

        faker = Faker()
        collects = []
        
        users_ids = [user["id"] for user in User.objects.all().values('id')]
        
        for _ in range(count_collects):
            collects.append(Collect(
                author = User.objects.filter(pk=faker.random_choices(users_ids, 1)[0]).get(),
                name = faker.name(),
                reason = faker.random_choices([elem[0] for elem in Collect.Reasons.choices], 1)[0],
                description = faker.text(255),
                money_target = random.randint(10_000, 99_999_999_999_999)/100 if random.randint(0, 1) else None,
            ))
        
        Collect.objects.bulk_create(collects)
        GREEN = self.style.HTTP_REDIRECT
        self.stdout.write(GREEN('Сборы созданы.'))