from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
import random
from faker import Faker

from cash_collection_app.models import Payment, Collect


class Command(BaseCommand):
    help = 'Create fake payments'

    def handle(self, *args, **options):
        count_payments = int(input('Введите кол-во оплат: '))
        if count_payments <= 0:
            raise CommandError('Кол-во оплат не может быть меньше 0')

        faker = Faker()
        payments = []
        
        users_ids = [user["id"] for user in User.objects.all().values('id')]
        collects_ids = [collect["id"] for collect in Collect.objects.all().values('id')]
        
        for _ in range(count_payments):
            payments.append(Payment(
                user = User.objects.filter(pk=faker.random_choices(users_ids, 1)[0]).get(),
                amount = random.randint(100, 9_999_999)/100,
                description = faker.text(255),
                collect = Collect.objects.filter(pk=faker.random_choices(collects_ids, 1)[0]).get(),
            ))

        Payment.objects.bulk_create(payments)
        for collect in Collect.objects.all():
            collect.save()
        GREEN = self.style.HTTP_REDIRECT
        self.stdout.write(GREEN('Оплаты созданы.'))