from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from faker import Faker


class Command(BaseCommand):
    help = 'Create fake users'

    def handle(self, *args, **options):
        count_users = int(input('Введите кол-во пользователей: '))
        if count_users <= 0:
            raise CommandError('Кол-во пользователей не может быть меньше 0')

        faker = Faker()
        users = []
        
        usernames_are_busy = [user.username for user in User.objects.all()]
        emails_are_busy = [user.email for user in User.objects.all()]

        for _ in range(count_users):
            email = faker.email()
            username = faker.user_name()
            
            max_possible_trying = 10
            count_trying = 0
            while email in emails_are_busy:
                count_trying += 1
                if count_trying > max_possible_trying:
                    raise CommandError('Походу у библиотеки faker закончились уникальные значения email')
                email = faker.email()
            emails_are_busy.append(email)
            
            count_trying = 0
            while username in usernames_are_busy:
                count_trying += 1
                if count_trying > max_possible_trying:
                    raise CommandError('Походу у библиотеки faker закончились уникальные значения username')
                username = faker.user_name()
            usernames_are_busy.append(username)
                
            users.append(
                User(email=email,
                     password=make_password(faker.password(
                        length=10, special_chars=True, digits=True, upper_case=True, lower_case=True)),
                     first_name=faker.first_name(),
                     last_name=faker.last_name(),
                     username=username,
                )
            )
            
        User.objects.bulk_create(users)
        GREEN = self.style.HTTP_REDIRECT
        self.stdout.write(GREEN('Пользователи созданы.'))