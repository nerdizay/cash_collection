from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

# Команда: python manage.py create_me --username=username --email=email@gmail.com --password=password
class Command(BaseCommand):
    help = 'create me'
    
    def add_arguments(self, parser):
        parser.add_argument('--username', type=str, default='test')
        parser.add_argument('--email', type=str, default='test@gmail.com')
        parser.add_argument('--password', type=str, default='test')

    def handle(self, *args, **kwargs):
        
        GREEN = self.style.HTTP_REDIRECT
        
        username = kwargs['username']
        email = kwargs['email']
        password = kwargs['password']
        hash_password = password=make_password(password)
        
        User(username=username, email=email, password=hash_password).save()
        
        self.stdout.write(GREEN('Вы созданы!'))
        self.stdout.write(f"username: {GREEN(username)}\
            \nemail: {GREEN(email)}\
                \npassword: {GREEN(password)}")