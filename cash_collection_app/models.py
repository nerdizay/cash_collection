from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from cash_collection_project.app_celery import (
    notify_about_success_collection_task,
    notify_about_success_payment_task,
)
from decimal import Decimal

class Payment(models.Model):
    date_time = models.DateTimeField(auto_now_add=True, verbose_name="дата и время")
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name="пользователь")
    amount = models.DecimalField(max_digits=14,
                                 decimal_places=2,
                                 validators=[MinValueValidator(Decimal("1.0"))],
                                 verbose_name="размер оплаты")
    description = models.CharField(max_length=255, null=True, verbose_name="описание")
    collect = models.ForeignKey("Collect", on_delete=models.PROTECT, verbose_name="сбор")
    
    class Meta:
        db_table = "payment"
        verbose_name = "оплата"
        verbose_name_plural = "оплаты"
        ordering = ["-id"]
        
    def save(self, *args, **kwargs) -> None:
        notify_about_success_payment_task.delay({
            "username": self.user.username,
            "email": self.user.email
        })
        super().save(*args, **kwargs)
        self.collect.count_participant += 1
        self.collect.money_now += self.amount
        self.collect.save()
        
    def __str__(self):
        return f"№{self.id} сбора {self.collect.name} пользователем {self.user.username} от {self.date_time.strftime('%H:%M %d.%m.%Y')}"



class Collect(models.Model):
    class Reasons(models.TextChoices):
        WEDDING = "wedding", "wedding"
        BIRTHDAY = "birthday", "birthday"
        
    author = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name="автор")
    name = models.CharField(max_length=255, verbose_name="название сбора")
    reason = models.CharField(max_length=255, choices=Reasons.choices, verbose_name="причина сбора")
    description = models.CharField(max_length=255, verbose_name="описание")
    money_target = models.DecimalField(max_digits=14,
                                       decimal_places=2,
                                       null=True,
                                       blank=True,
                                       validators=[MinValueValidator(Decimal("1.0"))],
                                       verbose_name="цель в денежном эквиваленте")
    
    money_now = models.DecimalField(max_digits=14, decimal_places=2, default=0.0, verbose_name="собрано")
    
    count_participant = models.IntegerField(default=0, verbose_name="кол-во неравнодушных")
    poster = models.ImageField(upload_to='static/images', null=True, blank=True, verbose_name="постер")
    date_time_ending = models.DateTimeField(null=True, verbose_name="дата и время, когда сбор заканчивается")
    
    class Meta:
        db_table = "collect"
        verbose_name = "сбор"
        verbose_name_plural = "сборы"
        ordering = ["-id"]
    
    def save(self, *args, **kwargs) -> None:
        if self.id is None:
            notify_about_success_collection_task.delay({
                "username": self.author.username,
                "email": self.author.email
            })
        super().save(*args, **kwargs)
        
    def __str__(self):
        return f"№{self.id} {self.author.username} \
            | название: {self.name} \
            | денежная цель: {self.money_target if self.money_target else 'нет'} \
            | описание: {self.description} \
            | дата и время окончания: {self.date_time_ending.strftime('%H:%M %d.%m.%Y') if self.date_time_ending else 'нет'}"