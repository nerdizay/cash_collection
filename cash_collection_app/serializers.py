from rest_framework import serializers
from .models import Payment, Collect
from rest_framework import status

class CustomPermissionError(serializers.ValidationError):
    status_code = status.HTTP_403_FORBIDDEN


class CollectSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Collect
        fields = '__all__'
        extra_kwargs = {'money_target': {'required': False}}
        read_only_fields = ('author', 'poster', 'money_now', 'count_participant')

    def create(self, validated_data):
        author = self.context['request'].user
        validated_data['author'] = author
        return super().create(validated_data)
    
    def update(self, instance, validated_data):
        if instance.author != self.context['request'].user:
            raise CustomPermissionError(detail="You are not author of this collect")
        # Если всё-таки можно будет редактировать сбро не только создателю, то тогда использовать эту строчку:
        # validated_data['author'] = instance.author , чтобы не менять автора
        return super().update(instance, validated_data)
    
class PaymentSerializer(serializers.ModelSerializer):
    # Редактирование не добавляю, потому что.. это же оплата.
    
    class Meta:
        model = Payment
        fields = '__all__'
        extra_kwargs = {'description': {'required': False}}
        read_only_fields = ('date_time', 'user')
        
    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user
        return super().create(validated_data)
    
