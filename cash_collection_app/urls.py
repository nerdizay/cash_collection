from django.urls import path
from .views import (
    PaymentListCreateView,
    CollectListCreateView,
    PaymentRetrieveView,
    CollectRetrieveUpdateView,
    NewslineListAPIView,
    upload_poster,
)

urlpatterns = [
    path('payments', PaymentListCreateView.as_view()),
    path('payments/<int:id>', PaymentRetrieveView.as_view()),
    path('collects', CollectListCreateView.as_view()),
    path('collects/<int:id>', CollectRetrieveUpdateView.as_view()),
    path('collects/<int:id>/newsline', NewslineListAPIView.as_view()),
    path('collects/<int:id>/upload/poster', upload_poster),
]
