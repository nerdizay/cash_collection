from django.core.files.temp import NamedTemporaryFile
from rest_framework import generics, status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.decorators import api_view, renderer_classes, permission_classes, authentication_classes
from rest_framework.renderers import (
    JSONRenderer,
    BrowsableAPIRenderer,
    MultiPartRenderer,
)
from .models import Payment, Collect, User
from .serializers import PaymentSerializer, CollectSerializer


class PaymentListCreateView(generics.ListCreateAPIView):
        
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PaymentSerializer

    def get_queryset(self):
        return Payment.objects.order_by('-id').all()
    
class CollectListCreateView(generics.ListCreateAPIView):
        
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = CollectSerializer

    def get_queryset(self):
        return Collect.objects.order_by('-id').all()
    
class PaymentRetrieveView(generics.RetrieveAPIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = 'id'
    serializer_class = PaymentSerializer
    
    def get_queryset(self):
        return Payment.objects.order_by('-id').all()
    
class CollectRetrieveUpdateView(generics.RetrieveUpdateAPIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = 'id'
    serializer_class = CollectSerializer
    
    def get_queryset(self):
        return Collect.objects.order_by('-id').all()

class NewslineListAPIView(generics.ListAPIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = 'collect__id'
    serializer_class = PaymentSerializer
    
    def get_queryset(self):
        return Payment.objects.order_by('-id').all()
    

@api_view(('POST','DELETE'))
@renderer_classes((JSONRenderer, BrowsableAPIRenderer, MultiPartRenderer))
@permission_classes((IsAuthenticated,))
@authentication_classes((BasicAuthentication, SessionAuthentication))
def upload_poster(request, id=None):
    collect = Collect.objects.filter(id=id)
    if not collect.exists():
        raise NotFound
    
    collect = collect.get()
    user = request.user
    
    if user != collect.author:
        raise PermissionDenied
    
    if request.method == "POST":
        file = request.FILES.get('file')
        with NamedTemporaryFile() as temp_file:
            for chunk in file.chunks():
                temp_file.write(chunk)
            collect.poster.delete(save=False)
            collect.poster.save(file.name, temp_file)
            
    elif request.method == "DELETE":
        collect.poster.delete(save=True)
    
    return Response({}, status=status.HTTP_200_OK)


