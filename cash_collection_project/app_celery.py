import os
from celery import Celery
from cash_collection_project.utils.email import notify_about_success_collection, notify_about_success_payment

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cash_collection_project.settings')

app = Celery('cash_collection_project', broker_connection_retry_on_startup=True)
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True, ignore_result=True)
def notify_about_success_collection_task(self, user):
    notify_about_success_collection(user)
    
@app.task(bind=True, ignore_result=True)
def notify_about_success_payment_task(self, user):
    notify_about_success_payment(user)