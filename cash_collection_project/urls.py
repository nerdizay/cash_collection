from django.contrib import admin
from django.urls import include, path
from cash_collection_project.settings import DEBUG


api_urls = (
    *include('cash_collection_app.urls'),
    #TODO: Сюда можно добавлять другие API
)

if DEBUG:
    from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView
    documentation = (
        path('docs/schema/', SpectacularAPIView.as_view(), name='schema'),
        path('docs/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
        path('docs/schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
    )
else:
    documentation = ()

urlpatterns = [
    path('admin/', admin.site.urls),
    *documentation,
    path('api/', api_urls),
]
