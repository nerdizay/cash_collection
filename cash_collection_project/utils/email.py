from django.template import Context, Template
from pydantic import BaseModel, ConfigDict
from pathlib import Path

from cash_collection_project.exceptions import MessageNotSent
from cash_collection_project.settings import (
    EMAIL_HOST_USER,
    NOTIFY_SUCCESS_PAYMENT_TEMPLATE,
    NOTIFY_SUCCESS_COLLECTION_TEMPLATE,
    EMAIL_HOST_PASSWORD,
    EMAIL_HOST,
    EMAIL_PORT,
    EMAIL_BACKEND
)

class MessageToEmail(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)
    
    title: str
    message: Template
    
class User(BaseModel):
    email: str
    username: str
    

class EmailSender:
    def __init__(self, message: MessageToEmail | None):
        if message is not None:
            self.message = message
        
    def set_message(self, message: MessageToEmail) -> None:
        self.message = message
    
    def inject_context(self, context):
        self.message.message = self.message.message.render(Context(context))
        
    def send(self, user):
        
        if self.message.message is None:
            raise MessageNotSent("Message not set")
        
        self.inject_context({'user': user.get('username')})
        
        try:
            import smtplib, ssl
            from email.message import Message

            msg = Message()

            msg["Subject"] = self.message.title
            msg["From"] = EMAIL_HOST_USER
            msg["To"] = user.get('email')

            msg.add_header('Content-Type','text/html')
            msg.set_payload(self.message.message)
            msg.set_charset('utf-8')
            
            context=ssl.create_default_context()

            with smtplib.SMTP(EMAIL_HOST, port=EMAIL_PORT) as smtp:
                smtp.starttls(context=context)
                smtp.login(msg["From"], EMAIL_HOST_PASSWORD)
                smtp.sendmail(msg['From'], msg['To'], msg.as_string())
                   
        except Exception as e:
            raise MessageNotSent(str(e))
        
        
def notify_about_something(path_to_template: Path, title: str):
    def notify_concrete(user):
        with open(path_to_template, 'r', encoding='utf-8') as f:
            content: str = f.read()
        message = MessageToEmail(title=title, message=Template(content))
        email_sender = EmailSender(message)
        try:
            email_sender.send(user)
        except MessageNotSent as e:
            #TODO: Настроить логгер
            print(f"Message not sent: {str(e)}")
    return notify_concrete
            
notify_about_success_payment = notify_about_something(NOTIFY_SUCCESS_PAYMENT_TEMPLATE, "Уведомление об оплате")
notify_about_success_collection = notify_about_something(NOTIFY_SUCCESS_COLLECTION_TEMPLATE, "Сбор создан")
