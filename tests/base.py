from django.test import TestCase, Client

class BaseTest(TestCase):
    fixtures = ['tests/fixtures/dumpdata.json']
    
    def setUp(self):
        self.url_collections = "/api/collects"
        self.url_payments = "/api/payments"
        self.client = Client()
        self.client.login(username='test', password='test')
    
    