from cash_collection_app.models import Collect
from tests.base import BaseTest
from decimal import Decimal

class TestAddPayment(BaseTest):
    
    def setUp(self):
        super().setUp()
        self.get_count_participant = lambda id: Collect.objects.filter(id=id).get().count_participant
        self.get_money_now = lambda id: Collect.objects.filter(id=id).get().money_now
    
    def test_count_participant(self):
        count_participant_before = self.get_count_participant(1)
        
        self.client.post(self.url_payments, data={
            'amount': "1000.0",
            'desctiption': 'some description',
            'collect': 1
        })
        
        self.assertEqual(self.get_count_participant(1),  count_participant_before + 1)
        
    def test_count_money_now(self):
        money_now_before = self.get_money_now(1)
        
        self.client.post(self.url_payments, data={
            'amount': "1000.0",
            'desctiption': 'some description',
            'collect': 1
        })
        
        self.assertEqual(self.get_money_now(1),  money_now_before + Decimal("1000.0"))
        
    def test_count_money_now_after_many_payments_by_one_time(self):
        money_now_before = self.get_money_now(1)
        
        for _ in range(1000):
            self.client.post(self.url_payments, data={
                'amount': "1000.0",
                'desctiption': 'some description',
                'collect': 1
            })
            
        self.assertEqual(self.get_money_now(1), money_now_before + Decimal("1000000.0"))
        
    def test_count_participant_after_many_payments_by_one_time(self):
        count_participant_before = self.get_count_participant(1)
        
        for _ in range(1000):
            self.client.post(self.url_payments, data={
                'amount': "1000.0",
                'desctiption': 'some description',
                'collect': 1
            })
            
        self.assertEqual(self.get_count_participant(1), count_participant_before + 1000)
