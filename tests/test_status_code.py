from tests.base import BaseTest
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from cash_collection_app import views
import json


class TestStatusCode401(BaseTest):
    
    def setUp(self):
        self.factory = RequestFactory()
        return super().setUp()
    
    def test_get_list_collections(self):
        request = self.factory.get(self.url_collections)
        request.user = AnonymousUser()
        
        response = views.CollectListCreateView.as_view()(request)
        
        self.assertIn(response.status_code, [401, 403])
        
    def test_create_collection(self):
        request = self.factory.post(self.url_collections)
        request.user = AnonymousUser()
        
        response = views.CollectListCreateView.as_view()(request)
        
        self.assertIn(response.status_code, [401, 403])
        
    def test_get_one_collection(self):
        request = self.factory.get(self.url_collections + "/1")
        request.user = AnonymousUser()
        
        response = views.CollectRetrieveUpdateView.as_view()(request)
        
        self.assertIn(response.status_code, [401, 403])
        
    def test_change_collection_full(self):
        request = self.factory.put(self.url_collections + "/1")
        request.user = AnonymousUser()
        
        response = views.CollectRetrieveUpdateView.as_view()(request)
        
        self.assertIn(response.status_code, [401, 403])
        
    def test_change_collection_part(self):   
        request = self.factory.patch(self.url_collections + "/1")
        request.user = AnonymousUser()
        
        response = views.CollectRetrieveUpdateView.as_view()(request)
        
        self.assertIn(response.status_code, [401, 403])
        
    def test_get_list_payments(self):
        request = self.factory.get(self.url_payments)
        request.user = AnonymousUser()
        
        response = views.PaymentListCreateView.as_view()(request)
        
        self.assertIn(response.status_code, [401, 403])
        
    def test_create_payment(self):
        request = self.factory.post(self.url_payments)
        request.user = AnonymousUser()
        
        response = views.PaymentListCreateView.as_view()(request)
        
        self.assertIn(response.status_code, [401, 403])

    def test_get_one_payment(self):
        request = self.factory.get(self.url_payments + "/1")
        request.user = AnonymousUser()
        
        response = views.PaymentRetrieveView.as_view()(request)
        
        self.assertIn(response.status_code, [401, 403])
        
class TestStatusCode201(BaseTest):
    
    def test_create_collection(self):
        response = self.client.post(self.url_collections, data={
            "name": "Scott Rhodes",
            "reason": "wedding",
            "description": "Land win occur loss. Politics against improve responsibility during soon suddenly. Unit feeling despite space.\nRise method do. Race paper wish top southern enough.\nFull purpose without seat effort. Green form certain theory nor talk people.",
        })
        
        self.assertEqual(response.status_code, 201)
        
    def test_create_payment(self):
        response = self.client.post(self.url_payments, data={
            'amount': 1000.0,
            'desctiption': 'some description',
            'collect': 1
        })

        self.assertEqual(response.status_code, 201)
        
class TestStatusCode200(BaseTest):
    
    def test_get_collections(self):
        response = self.client.get(self.url_collections)
        
        self.assertEqual(response.status_code, 200)
        
    def test_get_payments(self):
        response = self.client.get(self.url_payments)
        
        self.assertEqual(response.status_code, 200)
        
    def test_get_one_payment(self):
        response = self.client.get(self.url_payments + "/1")
        
        self.assertEqual(response.status_code, 200)
        
    def test_get_one_collection(self):
        response = self.client.get(self.url_collections + "/1")
        
        self.assertEqual(response.status_code, 200)
        
    def test_full_update_collection(self):
        response = self.client.post(self.url_collections, data={
            "name": "Scott Rhodes",
            "reason": "wedding",
            "description": "Land win occur loss. Appearance against",
            "money_target": "154221788.0",
            "date_time_ending": "2023-12-31T23:59:59"
        }, content_type="application/json")
        response = json.loads(response.content)
        collection_id = response['id']
        
        response = self.client.put(self.url_collections + f"/{collection_id}", data={
            "name": "i want money",
            "reason": "wedding",
            "description": "Land win occur loss. Politics against",
            "money_target": "154221788.0",
            "date_time_ending": "2024-12-31T23:59:59"
        }, content_type="application/json")
        
        self.assertEqual(response.status_code, 200)
        
    def test_part_update_collection(self):
        response = self.client.post(self.url_collections, data={
            "name": "Scott Rhodes",
            "reason": "wedding",
            "description": "Land win occur loss. Appearance against",
            "money_target": "154221788.0",
            "date_time_ending": "2023-12-31T23:59:59"
        }, content_type="application/json")
        response = json.loads(response.content)
        collection_id = response['id']
        
        response = self.client.patch(self.url_collections + f"/{collection_id}", data={
            "description": "Land win occur loss. Politics against"
        }, content_type="application/json")
        
        self.assertEqual(response.status_code, 200)
        
    def test_get_newsline(self):
        response = self.client.get(self.url_collections + "/1/newsline")
        
        self.assertEqual(response.status_code, 200)
        
    #TODO: Сэмитировать загрузку постера
    #TODO: Добавить удаление постера
    
class TestStatusCode403(BaseTest):
    
    def test_change_collection_full(self):
        #TODO: В идеале добавить Arrange, чтобы сбор точно не принадлежал клиенту
        response = self.client.put(self.url_collections + "/1", data={
            "name": "Scott Rhodes",
            "reason": "wedding",
            "description": "Land win occur loss. Appearance against",
            "money_target": "15422131",
            "date_time_ending": "2021-12-31T23:59:59"
        }, content_type="application/json")
        
        self.assertEqual(response.status_code, 403)
        
    def test_change_collection_part(self):
        #TODO: В идеале добавить Arrange, чтобы сбор точно не принадлежал клиенту
        response = self.client.patch(self.url_collections + "/1", data={
            "description": "Land win occur loss. Appearance against"
        }, content_type="application/json")
        
        self.assertEqual(response.status_code, 403)
        
    def test_upload_poster(self):
        response = self.client.post(self.url_collections + "/1/upload/poster", content_type="application/json")
        
        self.assertEqual(response.status_code, 403)
